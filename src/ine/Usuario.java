package ine;

public class Usuario {
	private String nombre,apePat, apeMat,domicilio, NombreEstado, curp, claveElector, sexo, fechaNac;
	private int id, estado, mz, LT, smza, localidad, municipio, emision, seccion, vigencia, aņoRegistro, codpostal;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombreEstado() {
		return NombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		NombreEstado = nombreEstado;
	}
	public int getMz() {
		return mz;
	}
	public void setMz(int mz) {
		this.mz = mz;
	}
	public int getLT() {
		return LT;
	}
	public void setLT(int lT) {
		LT = lT;
	}
	public int getSmza() {
		return smza;
	}
	public void setSmza(int smza) {
		this.smza = smza;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApePat() {
		return apePat;
	}
	public void setApePat(String apePat) {
		this.apePat = apePat;
	}
	public String getApeMat() {
		return apeMat;
	}
	public void setApeMat(String apeMat) {
		this.apeMat = apeMat;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getClaveElector() {
		return claveElector;
	}
	public void setClaveElector(String claveElector) {
		this.claveElector = claveElector;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getLocalidad() {
		return localidad;
	}
	public void setLocalidad(int localidad) {
		this.localidad = localidad;
	}
	public int getMunicipio() {
		return municipio;
	}
	public void setMunicipio(int municipio) {
		this.municipio = municipio;
	}
	public int getCodpostal() {
		return codpostal;
	}
	public void setCodpostal(int codpostal) {
		this.codpostal = codpostal;
	}
	public int getEmision() {
		return emision;
	}
	public void setEmision(int emision) {
		this.emision = emision;
	}
	public int getSeccion() {
		return seccion;
	}
	public void setSeccion(int seccion) {
		this.seccion = seccion;
	}
	public int getVigencia() {
		return vigencia;
	}
	public void setVigencia(int vigencia) {
		this.vigencia = vigencia;
	}
	public int getAņoRegistro() {
		return aņoRegistro;
	}
	public void setAņoRegistro(int aņoRegistro) {
		this.aņoRegistro = aņoRegistro;
	}
	
	@Override
	public String toString() {
		return "NOMBRE \n"+ apePat+ "          			           "+ "FECHA DE NACIMIENTO" +"\n"+apeMat+"          			    		       "+fechaNac+"\n" + nombre+"          			        	    "+"SEXO "+sexo	+ "\n"+ "DOMICILIO "+"\n"+ domicilio +"MZA "+ mz+"lt "+ LT +"\n" + "SUPMZA " +smza + " "+ codpostal +"\n"+ NombreEstado+ "\n" +"CLAVE DE ELECTOR " + claveElector+ "\n"+ "CURP "+curp+ "   "+ "AŅO DE REGISTRO "+ aņoRegistro+ "\n"+ "ESTADO " + estado+" " + "MUNICIPIO "+ municipio+" "+ "SECCION " + seccion+ "\n"+"LOCALIDAD "+localidad+" "+"EMISION " +emision+" " +"VIGENCIA " +vigencia  ;
	}

}
