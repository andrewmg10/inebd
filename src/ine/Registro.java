package ine;

import java.util.ArrayList;
import java.util.List;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;




public class Registro {
	private ObjectContainer db= null;
	
	//abrir registro
	private void abrirRegistro() {
		db=Db4oEmbedded.openFile("ClasePadre_Abstracta");
	}
	
	//cerrar registro
	private void cerrarRegistro() {
		db.close();
	}
	
	//insertar un regitro
			//primero abre registro->almacena el dato->cierra registro
			public void insertarRegistro(Usuario U) {
				abrirRegistro();
				db.store(U);
				cerrarRegistro();
			}
			
			//seleccionar usuarios aņadidos
			public List<Usuario> seleccionarUsuarios(){
				abrirRegistro();
				ObjectSet listaUsuarios=db.queryByExample(Usuario.class);
				List<Usuario> lp = new ArrayList<>();
				for(Object listaUsuarios1:listaUsuarios) {
					lp.add((Usuario)listaUsuarios1);
				}
				cerrarRegistro();
				return lp;
			}
			
			//parte 2 de seleccionar usuarios mostrando resultado
			public Usuario seleccionarUsuario(Usuario U) {
				abrirRegistro();
				ObjectSet resultado = db.queryByExample(U);
				Usuario usuario = (Usuario) resultado.next();
				cerrarRegistro();
				return usuario;
			}
			
			//actualizar el registro de usuarios 
			//llama valores->abre registro->inserta los nuevos en preresultado
			//->los guarda en la DB->cierra resgistro
			public void actualizarRegistro
			(String nombre, String apepat, String apemat, String domicilio,
					String nomEstado, String curp, String claveElec, String sexo, 
					String fechaNac,int id,int estado, int mz, int lt, int smza, 
					int municipio,int emision, int seccion, int localidad, int codpost, 
					int vigencia, int yearRegis) {
				
				abrirRegistro();
				Usuario U = new Usuario();
				U.setId(id);
				ObjectSet resultado = db.queryByExample(U);
				Usuario preresultado = (Usuario) resultado.next();				
				preresultado.setNombre(nombre);
				preresultado.setApePat(apepat);
				preresultado.setApeMat(apemat);
				preresultado.setDomicilio(domicilio);
				preresultado.setNombreEstado(nomEstado);
				preresultado.setCurp(curp);
				preresultado.setClaveElector(claveElec);
				preresultado.setSexo(sexo);
				preresultado.setMunicipio(municipio);
				preresultado.setEmision(emision);
				preresultado.setSeccion(seccion);
				preresultado.setVigencia(vigencia);
				preresultado.setAņoRegistro(yearRegis);
				preresultado.setLocalidad(localidad);
				preresultado.setEstado(estado);
				preresultado.setMz(mz);
				preresultado.setLT(lt);
				preresultado.setSmza(smza);
				preresultado.setCodpostal(codpost);
				preresultado.setFechaNac(fechaNac);
				db.store(preresultado);
				cerrarRegistro();
				}
			
			//eliminar registros
			public void eliminarRegistro(int id) {
				abrirRegistro();
				Usuario U = new Usuario();
				U.setId(id);
				ObjectSet resultado = db.queryByExample(U);
				Usuario preresultado = (Usuario) resultado.next();
				db.delete(preresultado);
				cerrarRegistro();
				
				
			}
}
